# CSL Visual and Law Editor


This web application allows users of CSL based reference managers (as [Zotero](https://www.zotero.org/)) to search for citation styles and edit them. In particular it simplifies editing styles for the German Law.

## Usage

You can acces the running implementation here : <https://csleditor.gitlab.io/csl-law-editor>

## Developer Informations

This Repository contains an implementation for the csl-editor-lib (see <https://gitlab.com/csleditor/csl-editor-lib>). That means all the technical details are in the submodule named csl-editor-lib. This Repository only contains details for the frontend. To develop, build and deploy, you will need this repository including all submodules.
This project is structured into three repositories. The first one is the csl-law-editor. This repository encloses the front end for the Website. It produces the structure of the website and loads the logic and the computational parts. These parts are in a second Repository cslEditorlib, which is this repository. cslEditorlib is integrated into the first repository as submodule. The third repository generates necessary Data for the Law Editor (translationData). It is integrated with a submodule under external/translationData. 

### Prerequisites
 The installation was tested on a fresh Ubuntu installation (18.04 LTS), but should also work with other systems.

  - Clone the Project with ``git clone git@gitlab.com:csleditor/csl-law-editor.git --recursive``
  - Install Jekyll with Ruby Gem: (taken from <https://jekyllrb.com/docs/installation/ubuntu/>). You will find instructions for other systems [here](https://jekyllrb.com/docs/installation/).
	- ``sudo apt-get install ruby-full build-essential zlib1g-dev``
	- ``echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc``
	- ``echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc``
	- ``echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc``
	- ``source ~/.bashrc ``
	- ``gem install jekyll bundler``

### Build and Serve

You can now build the Editor with the command ``bundle exec jekyll build``. If you like to wtach the generated Website in your brower type instead of ``build`` the command  ``serve``. It is pretty heplful to give also the option ``--watch``, so jekyll automatically updates whenever files changed.

 - To view the served site, point your browser to <http://127.0.0.1:4000/csl-editor/>. 
 - To run the unit Tests,  point your browser to <http://127.0.0.1:4000/csl-editor/cslEditorLib/pages/unitTests.html>. 




<!--
## To Deploy
TODO not really sure why needed

This process creates a static HTML site with concatenated javascript files and cache busters on the URLs, and optionally pushes to the `gh-pages` branch, currently served by github at [http://editor.citationstyles.org](http://editor.citationstyles.org).

- Run `git clone --recursive https://github.com/citation-style-editor/csl-editor-demo-site.git csl-demo` to checkout the repo.

- From the repo directory, run `./deploy.sh $BUILD_DIR $GH_PAGES_REPO_DIR`, where:
  - `$BUILD_DIR` is the name of the directory you wish to deploy to, relative to the parent of the current directory. **All current contents of** `$BUILD_DIR` **will be removed!**
  - `$GH_PAGES_REPO_DIR` (optional) is the name of a checked out `csl-editor-demo-site` repo directory, again relative to the parent of the current directory, which will be used to copy the built version and push the result to the `gh-pages` branch in github, which will automatically update the site at [editor.citationstyles.org](http://editor.citationstyles.org), the domain given in the CNAME file.

- Point your browser to `http://editor.citationstyles.org/cslEditorLib/pages/unitTests.html` to run the unit tests

- Point your browser to `http://editor.citationstyles.org` to view the deployed site


## Customising the core library

See documentation for the core library code and it's API at the [CSLEditorLib wiki](https://github.com/citation-style-editor/csl-editor/wiki).

If you fix bugs or otherwise improve the core [cslEditorLib](https://github.com/citation-style-editor/csl-editor) library, ensure the changes are not specific to your implementation and please issue a [pull request](https://github.com/citation-style-editor/csl-editor/pulls) so that everyone can benefit. Thanks!
-->

